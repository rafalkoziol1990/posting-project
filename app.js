var myApp = angular.module('myApp', ['ngRoute', 'ngResource', 'ngMaterial', 'ngMessages', 'ngAnimate'])
    .config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
        $routeProvider.when('/home', {
            templateUrl: './components/postings/views/posting.html',
            controller: 'PostingController',
            // controllerAs: 'store',
            reloadOnSearch: false
        });
        $routeProvider.when('/home/new', {
            templateUrl: './components/postings/views/postingAdd.html',
            controller: 'PostingAddController',
            // controllerAs: 'store',
            reloadOnSearch: false
        });
        $routeProvider.otherwise({
            redirectTo: '/home',
            //template:'<h1>NoneX</h1><p>Nothing has been selected</p>',
            resolve: () => {
                alert('yeeees')
            }
        });
        // unhash here and add <base href="?" in index.html head to enable html5 link style 
        // $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: false
        // });
    }])