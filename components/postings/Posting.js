class Posting {
    constructor(
        title = 'xxx',
        company = '',
        salaryMin = 0,
        salaryMax = 0,
        city = '',
        street = '',
        postalCode = ''
    ) {
        this.title = title;
        this.company = company;
        this.salaryMin = salaryMin;
        this.salaryMax = salaryMax;
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
    }
}