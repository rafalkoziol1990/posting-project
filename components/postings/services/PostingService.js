myApp.factory('PostingService', [function () {
    return {
        getRows: getRows,
        setRows: setRows
    }

    function getRows() {
        let postings = localStorage.getItem('postings');
        if (postings === null) {
            return [];
        }
        return JSON.parse(postings);
    }

    function setRows(posting) {
        let postings = getRows();
        postings.push(posting);
        console.log(postings);
        localStorage.setItem('postings', JSON.stringify(postings));
    };
}]);