myApp.directive('postingItem', function () {
    return {
        restrict: 'E',
        scope: {
            posting: '=info',
            fav: '&onFav'
        },
        template: `<md-card >
      <md-card-title>
          <md-card-title-text>
              <h2 class="no-margin">
                  <span>{{posting.title}}</span>
                  <span class="grey-colored italic">@ {{posting.company}}</span>
              </h2>
              <p>{{posting.city+' '+posting.postalCode+', '+posting.street}}</p>
          </md-card-title-text>
      </md-card-title>
      <md-card-actions layout="row" layout-align="end center">
          <md-button ng-click="addToFavourite($index)" class="md-icon-button md-accent" aria-label="Favorite">
              <i class="material-icons">favorite</i>
              <md-tooltip md-direction="left">Add to favourite</md-tooltip>
          </md-button>
      </md-card-actions>
  </md-card>`
    };
});