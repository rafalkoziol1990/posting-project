myApp.controller('PostingAddController', ['$scope', 'PostingService', function ($scope, PostingService) {
    $scope.title = 'Add new posting';
    $scope.posting = new Posting();
    $scope.isMaxSalaryOk = true;

    $scope.checkSalary = function () {
        console.log($scope.posting.salaryMin, $scope.posting.salaryMax)
        $scope.isMaxSalaryOk = $scope.posting.salaryMax > ($scope.posting.salaryMin * 2) ? false : true;
    }

    $scope.submit = (posting, isMaxSalaryOk) => {
        if (isMaxSalaryOk) {
            PostingService.setRows(posting);
        } else {
            alert('Data in form need to be corrected!')
        }


    }



}])